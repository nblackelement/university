import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Queue;


class WeatherService {

    Queue<Task> inQ = new LinkedList<>();
    Queue<Task> outQ = new LinkedList<>();

    void start() {

        // Run execute threads
        TaskExecutor taskExecutor = new TaskExecutor();
        Thread thr1 = new Thread(taskExecutor);
        Thread thr2 = new Thread(taskExecutor);
        Thread thr3 = new Thread(taskExecutor);
        thr1.start();
        thr2.start();
        thr3.start();

    }


    class TaskExecutor implements Runnable{

        private DataSource dataSource = new DataSourceImpl();

        void handle(Task task) {

            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            short weather = (short) dataSource.getWeather(task.getDate());
            task.setWeather(weather);

            //noinspection SynchronizeOnNonFinalField
            synchronized (outQ) {
                outQ.add(task);
            }
        }


        @Override
        public void run() {

            Task task;

            while (true){

                //noinspection SynchronizeOnNonFinalField
                synchronized (inQ) {

                    if (inQ.isEmpty())  continue;
                    task = inQ.remove();
                }

                handle(task);
            }

        }

    }


}



@SuppressWarnings("FieldCanBeLocal")
class Task {

    private short id;
    private String city = "Moscow";
    private LocalDate date;
    private short weather;


    Task(short id, LocalDate date) {

        this.id = id;
        this.date = date;
    }

    @Override
    public String toString() {
        return this.id + ": " + this.city + ", " + this.date + ", " + this.weather + "C";
    }


    void setWeather(short weather) {
        this.weather = weather;
    }

    short getId() {
        return id;
    }

    LocalDate getDate() {
        return date;
    }

}
