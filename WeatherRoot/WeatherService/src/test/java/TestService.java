import java.time.LocalDate;
import java.util.Random;

import org.junit.*;

import static org.junit.Assert.assertEquals;


public class TestService {

    private WeatherService ws;

    @Before
    public void init() {
        ws = new WeatherService();

        for (short i = 1; i < 6; i++) {
            Task task = new Task((short)0, LocalDate.of(2018, i, 15));
            TaskGenerator tg = new TaskGenerator(ws);
            tg.addElement(task);
        }
    }

    @Test
    public void inQSize() {
        assertEquals(5, ws.inQ.size());
        assertEquals(0, ws.outQ.size());
    }

    @Test
    public void outQSize() {
        ws.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(5, ws.outQ.size());
    }

    @Test
    public void taskTest() {
        ws.start();

        for (int i = 0; i < 5; i++) {

            try {
                Thread.sleep(350);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Task task;

            //noinspection SynchronizeOnNonFinalField
            synchronized (ws.outQ) {
                task = ws.outQ.remove();
            }

            assertEquals(0, task.getId());
            System.out.println(task);

        }

    }

    public static void main(String[] args) throws InterruptedException {

        WeatherService ws = new WeatherService();

        // Write thread
        Thread inThread = new Thread(new TaskGenerator(ws));
        inThread.start();

        ws.start();

        while (true){

            if (ws.outQ.isEmpty()) {
                Thread.sleep(350);

            } else {
                //noinspection SynchronizeOnNonFinalField
                synchronized (ws.outQ) {
                    System.out.println(ws.outQ.remove());
                }
            }
        }
    }


}



class TaskGenerator implements Runnable {

    private static short id;
    private Random random = new Random();
    private WeatherService ws;

    TaskGenerator(WeatherService ws) {
        this.ws = ws;
    }

    @Override
    public void run() {

        while (true){
            generateTask(5);

            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    private void generateTask(int taskCount) {

        for (short i = 1; i < taskCount + 1; i++) {

            int year = random.nextInt(10) + 2008;
            int month = random.nextInt(11) + 1;
            int day = random.nextInt(27) + 1;
            id++;

            Task task = new Task(id, LocalDate.of(year, month, day));
            addElement(task);
        }

    }

    void addElement(Task task) {

        //noinspection SynchronizeOnNonFinalField
        synchronized (ws.inQ) {
            ws.inQ.add(task);
        }
    }

}
