import java.time.LocalDate;

public interface DataSource {

    int getWeather(LocalDate date);
}
