import java.time.LocalDate;
import java.util.Random;


public class DataSourceImpl implements DataSource {

    private Random random = new Random();

    @Override
    public int getWeather(LocalDate date) {
        return getWeatherImpl(date);
    }


    private int getWeatherImpl(LocalDate date) {

        String month = date.getMonth().name();
        int temperature;

        switch(month) {
            case "JANUARY":
                temperature = random.nextInt(6)-10;
                break;

            case "FEBRUARY":
                temperature = random.nextInt(7)-10;
                break;

            case "MARCH":
                temperature = random.nextInt(8)-5;
                break;

            case "APRIL":
                temperature = random.nextInt(10)+2;
                break;

            case "MAY":
                temperature = random.nextInt(12)+8;
                break;

            case "JUNE":
                temperature = random.nextInt(11)+12;
                break;

            case "JULY":
                temperature = random.nextInt(11)+14;
                break;

            case "AUGUST":
                temperature = random.nextInt(11)+12;
                break;

            case "SEPTEMBER":
                temperature = random.nextInt(10)+7;
                break;

            case "OCTOBER":
                temperature = random.nextInt(7)+2;
                break;

            case "NOVEMBER":
                temperature = random.nextInt(5)-3;
                break;

            case "DECEMBER":
                temperature = random.nextInt(6)-8;
                break;

            default:
                temperature = 0;

        }


        return temperature;
    }


}
