package semester_4.lab_1;


public class TestCalc {
    public static void main(String[] args) {

        String type = "double";

        sumTest(type);
        subTest(type);
        multTest(type);
        divTest(type);
    }



    private static void sumTest(String type) {
        System.out.println("\nTesting of sum[" + type +"]...");


        switch (type) {

            case "int":

                if (Calc.sum(-12203, -55) != -12258) {
                    System.out.println("Failed. [-12203 + (-55)]");
                }

                if (Calc.sum(59, -22) != 37) {
                    System.out.println("Failed. [59 + (-22)]");
                }
                break;

            case "double":

                if (Calc.sum(5.5, 4.1) != 9.6) {
                    System.out.println("Failed. [5.5 + 4.1]");
                }

                if (Calc.sum(-1.9, 7.35) != 5.45) {
                    System.out.println("Failed. [-1.9 + 7.35]");
                }
                break;
        }

        if (Calc.sum(0, 0) != 0) {
            System.out.println("Failed. [0 + 0]");
        }
    }


    private static void subTest(String type) {
        System.out.println("\nTesting of sub[" + type + "]...");


        switch (type) {

            case "int":

                if (Calc.sub(5, 60) != -55) {
                    System.out.println("Failed. [5 - 60]");
                }

                if (Calc.sub(7, 2) != 5) {
                    System.out.println("Failed. [7 - 2]");
                }
                break;

            case "double":

                if (Calc.sub(8.2, 6.1) != 2.1) {
                    System.out.println("Failed. [8.2 - 6.1]");
                }

                if (Calc.sub(-5, 3.3) != -8.3) {
                    System.out.println("Failed. [-5 - 3.3]");
                }
                break;
        }

        if (Calc.sub(-1, 0) != -1) {
            System.out.println("Failed. [-1 - 0]");
        }
    }


    private static void multTest(String type) {
        System.out.println("\nTesting of mult[" + type + "]...");


        switch (type) {

            case "int":

                if (Calc.mult(2, 3) != 6) {
                    System.out.println("Failed. [2 * 6]");
                }

                if (Calc.mult(4, -2) != -8) {
                    System.out.println("Failed. [4 * (-2)]");
                }
                break;

            case "double":

                if (Calc.mult(2.2, 1.4) != 3.08) {
                    System.out.println("Failed. [2.2 * 1.4]");
                }

                if (Calc.mult(-0.8, 7.55) != -6.04) {
                    System.out.println("Failed. [-0.8 * 7.55]");
                }
                break;
        }

        if (Calc.mult(43, 0) != 0) {
            System.out.println("Failed. [43 * 0]");
        }
    }


    private static void divTest(String type) {
        System.out.println("\nTesting of div[" + type + "]...");


        switch (type) {

            case "int":

                if (Calc.div(24, 6) != 4) {
                    System.out.println("Failed. [24 / 6]");
                }

                if (Calc.div(-18, 3) != -6) {
                    System.out.println("Failed. [-18 / 3]");
                }
                break;

            case "double":

                if (Calc.div(2.0, 4.0) != 0.5) {
                    System.out.println("Failed. [2 / 4]");
                }

                if (Calc.div(7.6, -3.3) != -2.303) {
                    System.out.println("Failed. [7.6 / (-3.3)]");
                }
        }

        if (Calc.div(0, 10) != 0) {
            System.out.println("Failed. [0 / 10]");
        }
    }
}
