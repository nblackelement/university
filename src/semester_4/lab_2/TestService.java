package semester_4.lab_2;

import java.time.LocalDate;
import java.util.Queue;
import java.util.Random;

import static semester_4.lab_2.WeatherService.inQ;
import static semester_4.lab_2.WeatherService.outQ;


public class TestService {
    public static void main(String[] args) throws InterruptedException {

        test();

        // Write thread
        Thread inThread = new Thread(new TaskGenerator());

        // Run execute threads
        WeatherService.TaskExecutor taskExecutor = new WeatherService.TaskExecutor();
        Thread thr1 = new Thread(taskExecutor);
        Thread thr2 = new Thread(taskExecutor);
        Thread thr3 = new Thread(taskExecutor);
        thr1.start();
        thr2.start();
        thr3.start();


        // Testing
        thr1.join(1000);
        thr2.join(1000);
        thr3.join(1000);

        sizeAssert(outQ, 5);
        taskAssert();
        sizeAssert(outQ, 0);

        inThread.start();

        while (true){

            if (outQ.isEmpty()) {
                Thread.sleep(350);

            } else {
                //noinspection SynchronizeOnNonFinalField
                synchronized (outQ) {
                    System.out.println(outQ.remove());
                }
            }
        }

    }


    private static void test(){

        // Add new elements
        TaskGenerator.addElement(createTask(0, 2018, 5, 15));
        TaskGenerator.addElement(createTask(0, 2017, 4, 4));
        TaskGenerator.addElement(createTask(0, 2017, 2, 11));
        TaskGenerator.addElement(createTask(0, 2017, 5, 8));
        TaskGenerator.addElement(createTask(0, 2017, 10, 3));

        sizeAssert(inQ, 5);
    }

    private static Task createTask(int id, int year, int month, int day) {

        return new Task( (short)id, LocalDate.of(year, month, day));
    }

    private static void sizeAssert(Queue queue, int size) {

        if (queue.size() == size) {
            System.out.println("Queue size is correct.");

        } else {
            System.out.println("Size test FAILED...");
        }
    }

    private static void taskAssert() {

        while (!outQ.isEmpty()) {

            //noinspection SynchronizeOnNonFinalField
            synchronized (outQ) {
                Task task = outQ.remove();

                if (task.getId() == 0) {
                    System.out.println("Item found -> " + task);
                }
            }
        }
    }

}



class TaskGenerator implements Runnable {

    private static short id;
    private Random random = new Random();

    @Override
    public void run() {

        while (true){
            generateTask(5);

            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    private void generateTask(int taskCount) {

        for (short i = 1; i < taskCount + 1; i++) {

            int year = random.nextInt(10) + 2008;
            int month = random.nextInt(11) + 1;
            int day = random.nextInt(27) + 1;
            id++;

            Task task = new Task(id, LocalDate.of(year, month, day));
            addElement(task);
        }

    }

    static void addElement(Task task) {

        //noinspection SynchronizeOnNonFinalField
        synchronized (inQ) {
            inQ.add(task);
        }
    }

}
