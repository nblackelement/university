package semester_4.lab_2;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;


class WeatherService {

    static Queue<Task> inQ = new LinkedList<>();
    static Queue<Task> outQ = new LinkedList<>();


    static class TaskExecutor implements Runnable{

        Random random = new Random();

        void handle(Task task) {

            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            short temperature = (short)(random.nextInt(50) - 25);
            task.setWeather(temperature);

            //noinspection SynchronizeOnNonFinalField
            synchronized (outQ) {
                outQ.add(task);
            }
        }


        @Override
        public void run() {

            Task task;

            while (true){

                //noinspection SynchronizeOnNonFinalField
                synchronized (inQ) {

                    if (inQ.isEmpty())  continue;
                    task = inQ.remove();
                }

                handle(task);
            }

        }

    }


}



@SuppressWarnings("FieldCanBeLocal")
class Task {

    private short id;
    private String city = "Moscow";
    private LocalDate date;
    private short weather;


    Task(short id, LocalDate date) {

        this.id = id;
        this.date = date;
    }

    @Override
    public String toString() {
        return this.id + ": " + this.city + ", " + this.date + ", " + this.weather + "C";
    }


    void setWeather(short weather) {
        this.weather = weather;
    }

    short getId() {
        return id;
    }

}
