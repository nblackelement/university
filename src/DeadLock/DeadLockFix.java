package DeadLock;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static DeadLock.DeadLockFix.accountsList;
import static DeadLock.DeadLockFix.locksList;


public class DeadLockFix {

    static List<Account> accountsList = new ArrayList<>();
    static List<Lock> locksList = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException {

        // Create 10 accounts in the bankSimulator
        for (int i = 0; i < 10; i++) {
            accountsList.add(new Account(i, 1000));
            locksList.add(new ReentrantLock());
        }


        // Add tasks to ThreadPool
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        for (int i = 0; i < 100; i++) {
            executorService.submit(new Runner());
        }
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.HOURS);


        // Transfers report
        int sum = 0;

        for (Account account : accountsList)
            sum += account.getBalance();

        System.out.println(accountsList);
        System.out.println("Sum = " + sum);

    }


}


class Runner implements Runnable{

    @Override
    public void run() {

        Random random = new Random();

        // Random transfers
        int check = random.nextInt(100);
        short receiver = (short)(random.nextInt(10));
        short sender = (short)(random.nextInt(10));

        // Anti-DeadLock
        takeLocks(locksList.get(receiver), locksList.get(sender));

        try {

            BankSimulator.transfer(accountsList.get(receiver), accountsList.get(sender), check);

        } finally {
            locksList.get(receiver).unlock();
            locksList.get(sender).unlock();
        }

    }


    private void takeLocks(Lock lock1, Lock lock2) {

        boolean firstLockTaken = false;
        boolean secondLockTaken = false;

        while(true) {

            try {

                firstLockTaken = lock1.tryLock();
                secondLockTaken = lock2.tryLock();

            } finally {

                if (firstLockTaken && secondLockTaken) {
                    return;
                }

                if (firstLockTaken) {
                    lock1.unlock();
                }

                if (secondLockTaken) {
                    lock2.unlock();
                }
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
