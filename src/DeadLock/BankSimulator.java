package DeadLock;


class BankSimulator {

    // Singleton
    private BankSimulator() {

    }

    static void transfer(Account receiver, Account sender, int check) {

        sender.withdraw(check);
        receiver.deposit(check);
    }

}


class Account {
    private int id;
    private int balance;

    Account(int id, int balance) {
        this.id = id;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "[id: "+ id +"] = " + balance;
    }

    void withdraw(int amount) {
        balance -= amount;
    }

    void deposit(int amount) {
        balance += amount;
    }


    int getId() {
        return id;
    }

    int getBalance() {
        return balance;
    }

}
