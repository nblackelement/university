package DeadLock;

public class DeadLock {
    public static void main(String[] args) throws InterruptedException {

        Object lock1 = new Object();
        Object lock2 = new Object();

        Thread firstThread = new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < 100; i++) {

                    synchronized (lock1) {
                        synchronized (lock2) {

                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });

        Thread secondThread = new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < 100; i++) {

                    synchronized (lock2) {
                        synchronized (lock1) {

                            try {
                                Thread.sleep(999);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });

        firstThread.start();
        secondThread.start();

        // DeadLock.DeadLock
        firstThread.join();
        secondThread.join();

    }


}
